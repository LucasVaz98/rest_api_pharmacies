from database.db import *
from patient.PatientRepository import *
from pharmacy.PharmacyRepository import *
from transaction.TransactionRepository import *
from access_control.User import *
from access_control.Auth import *
from access_control.access_control import *
from fastapi import FastAPI

db_connection = init_connection()
app = FastAPI()


patient_repository = PatientRepository(db_connection)
pharmacy_repository = PharmacyRepository(db_connection)
transaction_repository = TransactionRepository(db_connection)


@app.post("/token")
def get_token(user: User):
    return generate_jwt(user.__dict__)


@app.post("/patients")
def get_all_patients(auth: Auth):
    if verify_jwt(auth.token):
        return patient_repository.get_all_patients()
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/patients/lastname/{patient_name}")
def get_patient_by_name(patient_name: str, auth: Auth):
    if verify_jwt(auth.token):
        return patient_repository.get_patient_by_last_name(patient_name)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/patients/uuid/{patient_id}")
def get_patient_by_id(patient_id: str, auth: Auth):
    if verify_jwt(auth.token):
        return patient_repository.get_patient_by_uuid(patient_id)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/pharmacies")
def get_pharmacies(auth: Auth):
    if verify_jwt(auth.token):
        return pharmacy_repository.get_all_pharmacies()
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/pharmacies/uuid/{uuid}")
def get_pharmacy_by_uuid(uuid: str, auth: Auth):
    if verify_jwt(auth.token):
        return pharmacy_repository.get_pharmacies_by_uuid(uuid)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/pharmacies/city/{city}")
def get_pharmacy_by_city(city: str, auth: Auth):
    if verify_jwt(auth.token):
        return pharmacy_repository.get_pharmacies_by_city(city)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/pharmacies/name/{name}")
def get_pharmacy_by_name(name: str, auth: Auth):
    if verify_jwt(auth.token):
        return pharmacy_repository.get_pharmacies_by_name(name)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/transactions")
def get_all_transactions(auth: Auth):
    if verify_jwt(auth.token):
        return transaction_repository.get_all_transactions()
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/transactions/pharmacy/{pharmacy_uuid}")
def get_transaction_by_pharmacy(pharmacy_uuid: str, auth: Auth):
    if verify_jwt(auth.token):
        return transaction_repository.get_transaction_by_pharmacy(pharmacy_uuid)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/transactions/patient/{patient_uuid}")
def get_transaction_by_patient(patient_uuid: str, auth: Auth):
    if verify_jwt(auth.token):
        return transaction_repository.get_transactions_by_patient(patient_uuid)
    else:
        return {"login_result": "Login Failed! Take token again!"}


@app.post("/transactions/uuid/{uuid}")
def get_transaction_by_uuid(uuid: str, auth: Auth):
    if verify_jwt(auth.token):
        return transaction_repository.get_transaction_by_uuid(uuid)
    else:
        return {"login_result": "Login Failed! Take token again!"}

@app.post("/show_tables")
def get_tables(auth: Auth):
    if verify_jwt(auth.token):
        return get_all_tables(db_connection)
    else:
        return {"login_result": "Login Failed! Take token again!"}
