import sqlite3


def init_connection():
    return sqlite3.connect("./database/backend_test.db", check_same_thread=False)


def get_all_tables(con):
    tables = []
    cur = con.cursor()

    for row in cur.execute("SELECT * FROM sqlite_master WHERE type='table';"):
        tables.append(row[1])
        print(row[1])

    return tables
