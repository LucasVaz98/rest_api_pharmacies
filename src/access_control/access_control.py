from jose import exceptions
from jose import jwt

ACCESS_TOKEN_EXPIRE_MINUTES = 30
ALGORITHM = "HS256"
JWT_SECRET_KEY = "6bef18936ac12a9096e9fe7a8fe1f777"

# USUÁRIO CRIADO PARA A VALIDAÇÃO DA API
user_test = {"user_name": "lucas",
              "password": "Ap1Teste"}

user_bsoft = {"user_name": "bluestorm",
              "password": "Software1324"}


def generate_jwt(user: {}) -> {}:
    data = user.copy()
    if validate_login(data.get("user_name"), data.get("password")):
        token_jwt = jwt.encode(data, JWT_SECRET_KEY, ALGORITHM)

        return {"user": user["user_name"], "token": token_jwt}
    else:
        return {"login_result": "Login Failed! Try again!"}


def validate_login(user: str, pswd: str) -> bool:
    if user == user_test["user_name"]:
            if pswd == user_test["password"]:
                return True

    elif user == user_test["user_name"]:
            if pswd == user_test["password"]:
                return True
    else:
        return False


def verify_jwt(token: str) -> bool:
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, [ALGORITHM])
        return validate_login(payload.get("user_name"), payload.get("password"))
    except exceptions.JWTError:
        return False
