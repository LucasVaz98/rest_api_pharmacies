class Transaction:
    def __init__(self, uuid, patient_id, pharmacie_id, amount, date_transaction):
        self.uuid = uuid
        self.patient_id = patient_id
        self.pharmacie_id = pharmacie_id
        self.amount = amount
        self.date_transaction = date_transaction
