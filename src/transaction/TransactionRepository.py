from .Transaction import *


class TransactionRepository:
    def __init__(self, db_con):
        self.db_connection = db_con

    def get_all_transactions(self) -> {}:
        transactions_list = []
        query = "SELECT * FROM transactions"

        for row in self.db_connection.cursor().execute(query):
            transactions_list.append(Transaction(row[0], row[1], row[2], row[3], row[4]))

        return transactions_list

    def get_transactions_by_patient(self, patient_uuid: str) -> {}:
        matches_list = []

        query = f"SELECT * FROM transactions WHERE patient_uuid = '{patient_uuid}'"

        for row in self.db_connection.cursor().execute(query):
            matches_list.append(Transaction(row[0], row[1], row[2], row[3], row[4]))

        return matches_list

    def get_transaction_by_pharmacy(self, pharmacy_uuid: str) -> {}:
        matches_list = []

        query = f"SELECT * FROM transactions WHERE pharmacy_uuid = '{pharmacy_uuid}'"

        for row in self.db_connection.cursor().execute(query):
            matches_list.append(Transaction(row[0], row[1], row[2], row[3], row[4]))

        return matches_list

    def get_transaction_by_uuid(self, transaction_uuid: str) -> {}:
        result = {}
        query = f"SELECT * FROM transactions WHERE uuid = '{transaction_uuid}'"

        for row in self.db_connection.cursor().execute(query):
            result = Transaction(row[0], row[1], row[2], row[3], row[4])

        return result
