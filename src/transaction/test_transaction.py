import unittest
import requests


class TestTransactionEndPoints(unittest.TestCase):
    def test_get_transaction_by_patient(self):
        cases = [{"input": "PATIENT0004", "result": [{"uuid": "TRAN0163",
                                                      "patient_id": "PATIENT0004",
                                                      "pharmacie_id": "PHARM0007",
                                                      "amount": 29.66,
                                                      "date_transaction": "2020-06-25 10:02:07.000000"},
                                                     {"uuid": "TRAN0291",
                                                      "patient_id": "PATIENT0004",
                                                      "pharmacie_id": "PHARM0003",
                                                      "amount": 24.88,
                                                      "date_transaction": "2021-07-17 04:28:36.000000"}]},
                 {"input": "PATIENT0013", "result": [{"uuid": "TRAN0106",
                                                      "patient_id": "PATIENT0013",
                                                      "pharmacie_id": "PHARM0002",
                                                      "amount": 9.28,
                                                      "date_transaction": "2021-02-15 23:20:58.000000"},
                                                     {"uuid": "TRAN0217",
                                                      "patient_id": "PATIENT0013",
                                                      "pharmacie_id": "PHARM0008",
                                                      "amount": 7.51,
                                                      "date_transaction": "2021-01-20 23:20:20.000000"}]}]

        for case in cases:
            self.assertEqual(request_transaction_by_patient(case["input"]), case["result"])

    def test_get_transaction_by_uuid(self):
        cases = [{"input": "TRAN0001", "result": {"uuid": "TRAN0001",
                                                  "patient_id": "PATIENT0045",
                                                  "pharmacie_id": "PHARM0008",
                                                  "amount": 3.5,
                                                  "date_transaction": "2020-02-05 07:49:03.000000"}},
                 {"input": "TRAN0006", "result": {"uuid": "TRAN0006",
                                                  "patient_id": "PATIENT0019",
                                                  "pharmacie_id": "PHARM0007",
                                                  "amount": 4.95,
                                                  "date_transaction": "2021-03-06 13:07:36.000000"}},
                 {"input": "TRAN0098", "result": {"uuid": "TRAN0098",
                                                  "patient_id": "PATIENT0032",
                                                  "pharmacie_id": "PHARM0010",
                                                  "amount": 4.62,
                                                  "date_transaction": "2021-02-01 02:36:01.000000"}}]

        for case in cases:
            self.assertEqual(request_transaction_by_uuid(case["input"]), case["result"])


def request_transaction_by_uuid(uuid: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/transactions/uuid/{uuid}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def request_transaction_by_patient(patient_uuid: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/transactions/patient/{patient_uuid}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def do_login():
    user = {"user_name": "lucas", "password": "Ap1Teste"}
    req = requests.post(url="http://0.0.0.0:5000/token", json=user)

    return {"token": req.json()["token"]}



if __name__ == "__main__":
    unittest.main()
