import unittest
import requests


class TestPatientEndPoints(unittest.TestCase):
    def test_get_patient_by_last_name(self):
        cases = [{"input": "MANCINI", "result": [{"uuid": "PATIENT0017",
                                                  "first_name": "ROGERIO",
                                                  "last_name": "MANCINI",
                                                  "date_birth": "1992-12-13 00:00:00.000000"},
                                                 {"uuid": "PATIENT0025",
                                                  "first_name": "ABEL",
                                                  "last_name": "MANCINI",
                                                  "date_birth": "1979-03-22 00:00:00.000000"},
                                                 {"uuid": "PATIENT0048",
                                                  "first_name": "CARLOS",
                                                  "last_name": "MANCINI",
                                                  "date_birth": "1991-12-13 00:00:00.000000"}]},
                 {"input": "DONEGA", "result": [{"uuid": "PATIENT0005",
                                                 "first_name": "LETICIA",
                                                 "last_name": "DONEGA",
                                                 "date_birth": "1978-02-27 00:00:00.000000"},
                                                {"uuid": "PATIENT0039",
                                                 "first_name": "VITORIA",
                                                 "last_name": "DONEGA",
                                                 "date_birth": "1984-11-04 00:00:00.000000"}]}]

        for case in cases:
            self.assertEqual(request_patient_by_last_name(case["input"]), case["result"])

    def test_get_patient_by_uuid(self) -> {}:
        cases = [{"input": "PATIENT0002", "result": {"uuid": "PATIENT0002",
                                                     "first_name": "GUSTAVO",
                                                     "last_name": "SALOMAO",
                                                     "date_birth": "1984-12-05 00:00:00.000000"}},
                 {"input": "PATIENT0007", "result": {"uuid": "PATIENT0007",
                                                     "first_name": "GUSTAVO",
                                                     "last_name": "PEREIRA",
                                                     "date_birth": "1987-08-17 00:00:00.000000"}},
                 {"input": "PATIENT0015", "result": {"uuid": "PATIENT0015",
                                                     "first_name": "CRISTIANO",
                                                     "last_name": "TEIXEIRA",
                                                     "date_birth": "1984-11-16 00:00:00.000000"}}]

        for case in cases:
            self.assertEqual(request_patient_by_uuid(case["input"]), case["result"])


def request_patient_by_last_name(last_name: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/patients/lastname/{last_name}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def request_patient_by_uuid(uuid: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/patients/uuid/{uuid}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def do_login():
    user = {"user_name": "lucas", "password": "Ap1Teste"}
    req = requests.post(url="http://0.0.0.0:5000/token", json=user)

    return {"token": req.json()["token"]}


if __name__ == "__main__":
    unittest.main()
