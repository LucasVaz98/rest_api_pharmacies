class Patient:
    def __init__(self, uuid: str, first_name: str, last_name: str, date_birth: str):
        self.uuid = uuid
        self.first_name = first_name
        self.last_name = last_name
        self.date_birth = date_birth
