from .Patient import *


class PatientRepository:

    def __init__(self, db_con):
        self.db_connection = db_con

    def get_all_patients(self) -> {}:
        patients_list = []

        for row in self.db_connection.cursor().execute("SELECT * FROM patients;"):
            patients_list.append(Patient(row[0], row[1], row[2], row[3]))
        return patients_list

    def get_patient_by_last_name(self, last_name: str) -> {}:
        matches = list()

        for row in self.db_connection.cursor().execute(f"SELECT * FROM PATIENTS WHERE LAST_NAME = '{last_name}';"):
            matches.append(Patient(row[0], row[1], row[2], row[3]))

        return matches

    def get_patient_by_uuid(self, uuid: str) -> {}:
        for row in self.db_connection.cursor().execute(f"SELECT * FROM patients WHERE uuid = '{uuid}';"):
            return Patient(row[0], row[1], row[2], row[3]).__dict__
