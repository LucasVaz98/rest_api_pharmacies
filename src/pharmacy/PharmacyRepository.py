from .Pharmacy import *


class PharmacyRepository:
    def __init__(self, db_con):
        self.db_connection = db_con

    def get_all_pharmacies(self) -> {}:
        pharmacies_list = []
        query = "SELECT * FROM pharmacies;"

        for row in self.db_connection.cursor().execute(query):
            pharmacies_list.append(Pharmacy(row[0], row[1], row[2]))

        return pharmacies_list

    def get_pharmacy_by_uuid(self, uuid: str) -> {}:
        query = f"SELECT * FROM pharmacies WHERE uuid = '{uuid}';"

        for row in self.db_connection.cursor().execute(query):
            result = Pharmacy(row[0], row[1], row[2])

        return result

    def get_pharmacies_by_city(self, city: str) -> {}:
        matches_list = []
        query = f"SELECT * FROM pharmacies WHERE city = '{city}';"

        for row in self.db_connection.cursor().execute(query):
            matches_list.append(Pharmacy(row[0], row[1], row[2]))

        return matches_list

    def get_pharmacies_by_name(self, name: str) -> {}:
        matches_list = []
        query = f"SELECT * FROM pharmacies WHERE name = '{name}';"

        for row in self.db_connection.cursor().execute(query):
            matches_list.append(Pharmacy(row[0], row[1], row[2]))

        return matches_list

    def get_pharmacies_by_uuid(self, uuid: str) -> {}:
        result = {}
        query = f"SELECT * FROM pharmacies WHERE uuid = '{uuid}';"

        for row in self.db_connection.cursor().execute(query):
            result = Pharmacy(row[0], row[1], row[2])

        return result
