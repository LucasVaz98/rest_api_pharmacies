import unittest
import requests


class TestPharmacyEndPoints(unittest.TestCase):
    def test_get_pharmacy_by_city(self):
        cases = [{"input": "CAMPINAS", "result": [{"uuid": "PHARM0008",
                                                   "name": "DROGAO SUPER",
                                                   "city": "CAMPINAS"},
                                                  {"uuid": "PHARM0009",
                                                   "name": "DROGASIL",
                                                   "city": "CAMPINAS"}]},
                 {"input": "SAO SIMAO", "result": [{"uuid": "PHARM0002",
                                                    "name": "DROGAO SUPER",
                                                    "city": "SAO SIMAO"},
                                                   {"uuid": "PHARM0007",
                                                    "name": "DROGASIL",
                                                    "city": "SAO SIMAO"}]}]

        for case in cases:
            self.assertEqual(request_pharmacy_by_city(case["input"]), case["result"])

    def test_get_pharmacy_by_name(self):
        cases = [{"input": "DROGARIA SAO SIMAO", "result": [{"uuid": "PHARM0005",
                                                             "name": "DROGARIA SAO SIMAO",
                                                             "city": "RIBEIRAO PRETO"},
                                                            {"uuid": "PHARM0010",
                                                             "name": "DROGARIA SAO SIMAO",
                                                             "city": "SAO PAULO"}]},
                 {"input": "DROGASIL", "result": [{"uuid": "PHARM0007",
                                                   "name": "DROGASIL",
                                                   "city": "SAO SIMAO"},
                                                  {"uuid": "PHARM0009",
                                                   "name": "DROGASIL",
                                                   "city": "CAMPINAS"}]}]

        for case in cases:
            self.assertEqual(request_pharmacy_by_name(case["input"]), case["result"])

    def test_get_pharmacy_by_uuid(self):
        cases = [{"input": "PHARM0006", "result": {"uuid": "PHARM0006",
                                                   "name": "DROGA MAIS",
                                                   "city": "SAO PAULO"}},
                 {"input": "PHARM0001", "result": {"uuid": "PHARM0001",
                                                   "name": "DROGA MAIS",
                                                   "city": "RIBEIRAO PRETO"}},
                 {"input": "PHARM0010", "result": {"uuid": "PHARM0010",
                                                   "name": "DROGARIA SAO SIMAO",
                                                   "city": "SAO PAULO"}}]

        for case in cases:
            self.assertEqual(request_pharmacy_by_uuid(case["input"]), case["result"])


def request_pharmacy_by_uuid(uuid: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/pharmacies/uuid/{uuid}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def request_pharmacy_by_name(name: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/pharmacies/name/{name}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def request_pharmacy_by_city(city: str) -> {}:
    access_token = do_login()
    url = f"http://0.0.0.0:5000/pharmacies/city/{city}"
    req = requests.post(url=url, json=access_token)

    return req.json()


def do_login():
    user = {"user_name": "lucas", "password": "Ap1Teste"}
    req = requests.post(url="http://0.0.0.0:5000/token", json=user)

    return {"token": req.json()["token"]}


if __name__ == "__main__":
    unittest.main()
