# Pharmacy, Patient and Transaction API


## Hello, welcome to this project! 

It's a simple REST Api implemented using Python 3.10 and some librarys as:
* FastAPI
* jose 
* sqlite3
* BaseModel from pydantic

The project implement a simple module of unit test for the principals endpoints using Unittest framework.

## Host on Local Machine

1. Open a new terminal and download source code or clone repo:

``` bash
    $ git clone https://gitlab.com/LucasVaz98/rest_api_pharmacies.git
```

2.  After that open a terminal into the folder you have downloaded the source code:

``` bash
    $ cd rest_api_pharmacies/
    $ sudo docker buildx build -t api/rest:1.0 .
``` 
After you type sudo's password if you see something like the next image that means the docker container is building an image under name api/rest:1.0 with the python code to host the API.

![docker_building_application](/home/lucas_vaz/Documents/api_rest/docker_building_image.png)

3. Now we can run this container image using the follow command line:


``` bash
    $ sudo docker run -p 5000:8000 api/rest:1.0
```

At this step you should see some INFO lines from uvicorn server, like this:

![uvicorn_server](/home/lucas_vaz/Documents/api_rest/server_uvicorn.png)


The container is now listening to any request coming to the address _http://0.0.0.0:8000_, those requests are mapped from port _5000_ on your local machine.  

## Running Automatic Tests

As said before, the project include tests implemented using Unittest framework. 

The end points for Patient, Pharmacy and Transaction can be tested individually by using the following commands:

``` bash
    $ python -m unittest src/patient/test_patient.py
    $ python -m unittest src/transaction/test_transaction.py
    $ python -m unittest src/pharmacy/test_pharmacy.pyS
```
But you can also test all endpoints for once, using the command:

``` bash
    $ python -m unittest discover
```
This way Unittest handles to search for tests and running them.


## Send Requests

With the server running we can also send requests to the severs, like so:

``` python
    import requests

    def request_patient_by_uuid(uuid: str, token: {}) -> {}:
        url = f"http://0.0.0.0:5000/patients/uuid/{uuid}"
        req = requests.post(url=url, json=token)

        return req.json()

    def login(user: str, password: str) -> {}:
        url = "http://0.0.0.0:5000/token/"
        user = {"user_name": user,
                "password": password}
        req = requests.post(url=url, json=user)

        return req.json()

    def main():
        access_token = login("lucas", "Ap1Teste")
        print(request_patient_by_uuid("PATIENT0001", access_token))

    if __name__ == "__main__":
        main()

```


This script request to the API a patient by uuid _PATIENT0001_

In the project is included a documentation at [swagger](http://0.0.0.0:5000/docs)
