FROM ubuntu

WORKDIR /
RUN mkdir rest_api
WORKDIR /rest_api

COPY . .

WORKDIR src

RUN apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-pip

RUN pip install  uvicorn
RUN pip install  fastapi
RUN pip install python-jose[cryptography]

EXPOSE 8000

CMD ["uvicorn", "app:app", "--reload", "--host", "0.0.0.0", "--port", "8000" ]

